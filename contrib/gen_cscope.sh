#/bin/sh

#
# Script to generate a Cscope database for the open64 compiler source tree.
# - Run from the top-level directory as "./contrib/gen_cscope.sh"
#

# go to / to store absolute paths in the cscope database
ROOT=`pwd`
cd / 	

# finds all *.c, *.h, *.cxx, and *.cpp files
find $ROOT \( -name "*.[chl]" -o -name "*.c[xp][xp]" \) >$ROOT/cscope.files 


### Build cscope database
# -q for inverted index (for fast symbol lookup)
# -b to build ref only: no interaction
# add -k if you don't want standard /usr/include files to be parsed
cd $ROOT 
cscope -q -b 



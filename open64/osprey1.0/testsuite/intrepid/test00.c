#include <upc_strict.h>
#include <stdio.h>

shared char x1;
shared short x2;
shared int x3;
shared long long x4;
shared float x5;
shared double x6;

void
test00()
{
  if(MYTHREAD == 0) {
      x1 = 255;
      x2 = -2;
      x3 = -3;
      x4 = -4; 
      x5 = -5.0;
      x6 = -6.0;
  }
    upc_barrier; 
  if (x1 != 255)
    abort ();
  if (x2 != -2)
    abort ();
  if (x3 != -3)
    abort ();
  if (x4 != -4)
    abort ();
  if (x5 != -5.0)
    abort ();
  if (x6 != -6.0)
    abort ();
   upc_barrier; 
  if (MYTHREAD == 0)
    {
      printf ("test00: access shared scalars - passed.\n");
    }
}

int
main()
{
  test00 ();
  exit (0);
}


#include <upc_strict.h>
#include <stdio.h>

shared char x1;
shared short x2;
shared int x3;
shared long long x4;


void
set_proc (shared char *p1, char v1, shared short *p2, short v2, shared int *p3,int v3, shared long long *p4, long long v4)
{
  *p1 = v1; 
  *p2 = v2;
  *p3 = v3;
  *p4 = v4;
}

void
get_proc (shared char *p1, char *v1, shared short *p2, short *v2, shared int *p3, int *v3, shared long long *p4, long long *v4)
{
  *v1 = *p1;
  *v2 = *p2;
  *v3 = *p3;
  *v4 = *p4;
}

void
test04()
{
  char xv1;
  short xv2;
  int xv3;
  long long xv4;
  if (MYTHREAD == 0)
     {
       set_proc(&x1, 255, &x2, -2, &x3, -3, &x4, -4);
    }
   upc_barrier; 
   get_proc(&x1, &xv1, &x2, &xv2, &x3, &xv3, &x4, &xv4);
  if (xv1 != 255)
    abort ();
  if (xv2 != -2)
    abort ();
  if (xv3 != -3)
    abort ();
  if (xv4 != -4)
    abort ();
    upc_barrier;  
  if (MYTHREAD == 0)
    {
      printf ("test04 (access shared values via (shared *) parameters) - passed.\n");
    }
}

int
main()
{
  test04 ();
  exit (0);
}

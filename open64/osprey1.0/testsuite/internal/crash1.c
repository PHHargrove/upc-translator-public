
#include "upc.h"

#pragma upc relaxed


shared int si; 

float f, f0;
shared float sf;
shared [3] float sf3;


shared int *pi;
shared [5] int *pi5;
int j;
strict shared int i;
int *lp;

shared [3] int *pi3;
extern void fsp(shared int *p); 



extern void fpi(int *p);


shared int array[100][400*THREADS];
int beta[100][400];
int gamma[10][20][30][40];


typedef struct {
int a[100];
int j;
} TI;

typedef  struct {
  double d[100];
  int a[25];
  TI es;
  int  i;
} T;

shared T ss, ss1, sa[10*THREADS], SA[10][10*THREADS];
shared T *sp, *spillsp;
T ns;
TI s;

shared void *pv;
extern void fspv(shared void* p);
void (*pfsp)(shared int *) = fsp;
extern void fsp3(shared [3] int *p);

extern void fspc(shared  char *p);
extern void fi(int x);
extern void fTI (TI s);

void main() {
  
  int j1, j2, j3, j4;
  double d1;

  /* j = i; */
/*   j = ss.i; */

  {
#pragma upc strict


 /* bug here - value numbering changes the tree and I lose
    the type information */
    j = array[j+45][j1+567];
   j1 = beta[j][j1];
    j = gamma[j1][j2][j3][j4];
    j1 = ss.es.a[j];
    *lp = sa[j1].a[j];
    d1 = ss.d[j];
    array[j+45][j1+567] = j;


    ns = ss;
    si = i;
    j = *(pi + j + j1);
    j1 = sp->es.a[j];
    spillsp = sp + j + j1;
    j2 = spillsp->es.a[j3];
    j2 = (sp + j + j1)->es.a[j3];
    j = array[j][j1];
    
    
    j1 = SA[j1][j2].a[j3];
    j2 = ss.es.a[j];
    j3 = sa[j1].es.a[j];
    

   
  }

  array[j][j1] = j;
  ss.es.a[j] = j;
  j = ss.es.j;
  j = ss.i;
  j = sa[j].i;
  j = sa[sa[j].a[j]].a[j]; 
  ss.a[j] = ss1.a[j];
  j = *pi;
  j = *pi5;
  j = sa[j].a[j];
  j = ss.a[j];
  s = ss.es; 	
  si = i;
  i = ss.i; 
  ss.es = s;
  i = j;
  sf = f;
  sf3 = f0;
  ss = ns;
  ss.i = j;

  
  ss.a[j] = j; 
  sa[j].a[j] = j;
  *pi = j;
  *(pi+1) = j;
  
   if (pi == &i )
    i = j;
  
   if( pi == 0)
     i = j;
   
   if( pi == pi5)
     i = j;
   if (pi == &i || pi == 0 || pi == pi5 || pi5 == 0)
     i = j;
   
   
   lp = pi;
   lp = pi5;
   i = si;
   ss.es = s;
   ss.es = ss1.es;
   ns.a[j] = j;
   j = ss.a[j];
   sa[j].a[j] = j;
 

  
   fsp(pi);
   fpi(pi);
   fpi(pi5);
   fsp(pi5);
   fsp3(pi);
   fsp3(pi3);
   fsp3(pi5);
   fspc(pi);
   fspc(pv);
   fspv(pi);
   fspv(pi5);
   fspv(pv);
   fsp3(pv);
   fi(ss.i);
   fi(ss.a[j]);
   fi(sa[j].i);
   fi(sa[j].a[j]);
   fi(sp->i);
   fi(sp->a[j]);
   
  
  fTI(ss.es);
  fTI(sa[j].es);
  fTI(sp->es);
  j = sa[j].a[j];
   j = ss.a[j];
   ss.a[j] = ss1.a[j];
   s = ss.es; 	
   j = ss.i;
   si = i;
   i = ss.i; 
   ss.es = s;
   array[j][j] = j;
}




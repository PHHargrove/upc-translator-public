#include <upc_strict.h>
#include <stdio.h>

struct data_struct
  {
    char x1;
    short x2;
    int x3;
    long long x4;
  };

shared struct data_struct s, s1;
struct data_struct l;
short jlo;

int main() {
  l = s;
  s = l;
  s = s1;
  s.x2 = -2;
  jlo = s.x2;
}
